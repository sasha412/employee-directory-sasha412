﻿namespace EmployeeDirectory.Features.Account
{
    using System.ComponentModel.DataAnnotations;
    using System.Linq;
    using System.Threading.Tasks;
    using AutoMapper;
    using FluentValidation;
    using MediatR;
    using Models;
    using BCrypt.Net;
    using Infrastructure;
    using Microsoft.AspNetCore.Mvc.ViewFeatures.Internal;
    using Microsoft.AspNetCore.Mvc;

    public class AccountLogin
    {
        public class Command : IRequest
        {
            [Display(Name = "Email:")]
            public string EmailAddress { get; set; }

            [Display(Name = "Password:")]
            [DataType(DataType.Password)]
            public string Password { get; set; }
        }

        public class Validator : AbstractValidator<Command>
        {
            private readonly DirectoryContext _database;

            public Validator(DirectoryContext database)
            {
                _database = database;
                RuleFor(employee => employee)
                    .Must(ValidatePassword).WithMessage("Incorrect Email and/or Password");
            }

            private bool ValidatePassword(Command command)
            {
                if (string.IsNullOrEmpty(command.EmailAddress) || string.IsNullOrEmpty(command.Password))
                    return false;

                var employee = _database.Employee.SingleOrDefault(x => x.EmailAddress == command.EmailAddress);
                if (employee == null)
                {
                    return false;
                }
                return BCrypt.Verify(command.Password, employee.HashedPassword);
            }
        }

        public class CommandHandler : RequestHandler<Command>
        {
            private readonly DirectoryContext _database;
            private readonly ILoginService _loginService;

            public CommandHandler(DirectoryContext database, ILoginService loginService)
            {
                _database = database;
                _loginService = loginService;
            }

            protected override async void HandleCore(Command command)
            {
                var employee = _database.Employee.SingleOrDefault(x => x.EmailAddress == command.EmailAddress);
                await _loginService.LogIn(employee.EmailAddress); 
            }
        }
    }
}
