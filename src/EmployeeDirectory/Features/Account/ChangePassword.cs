﻿namespace EmployeeDirectory.Features.Account
{
    using System.ComponentModel.DataAnnotations;
    using AutoMapper;
    using FluentValidation;
    using Models;
    using MediatR;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using BCrypt.Net;

    public class ChangePassword
    {
        public class Command : IRequest<Employee>
        {
            [Display(Name = "CurrentPassword")]
            [DataType(DataType.Password)]
            public string CurrentPassword { get; set; }

            [Display(Name = "New Password")]
            [DataType(DataType.Password)]
            public string NewPassword { get; set; }

            [Display(Name = "Confirm Password")]
            [DataType(DataType.Password)]
            public string ConfirmPassword { get; set; }
        }

        public class Validator : AbstractValidator<Command>
        {
            private readonly DirectoryContext _database;
            private readonly UserContext _userContext;

            public Validator(DirectoryContext database, UserContext userContext)
            {
                _database = database;
                _userContext = userContext;
                RuleFor(employee => employee.CurrentPassword).NotEmpty().Must(ValidateCurrentPassword).WithMessage("Current Password is invalid. Please enter your current password and try again."); ;
                RuleFor(employee => employee.NewPassword).NotEmpty().Length(6,16).WithMessage("Invalid new password.");
                RuleFor(employee => employee.ConfirmPassword).NotEmpty().Must(ValidateConfirmPassword).WithMessage("These passwords do not match. Be sure to enter the same password twice.");
            }

            private bool ValidateCurrentPassword(Command command,string currentPassword)
            {
                if (string.IsNullOrEmpty(currentPassword))
                    return false;              

                var employee = _database.Employee.SingleOrDefault(x => x.EmailAddress == _userContext.User.EmailAddress);
                if (employee == null)
                {
                    return false;
                }
                return BCrypt.Verify(currentPassword, employee.HashedPassword);
            }

            private bool ValidateConfirmPassword(Command command, string confirmPassword)
            {
                if (string.IsNullOrEmpty(confirmPassword) || string.IsNullOrEmpty(command.CurrentPassword))
                    return false;

                return confirmPassword == command.NewPassword;
            }
        }

        public class CommandHandler : RequestHandler<Command, Employee>
        {
            private readonly DirectoryContext _database;
            private readonly UserContext _userContext;
            public CommandHandler(DirectoryContext database, UserContext userContext)
            {
                _database = database;
                _userContext = userContext;
            }

            protected override Employee HandleCore(Command command)
            {
                var employee = _database.Employee.SingleOrDefault(x => x.EmailAddress == _userContext.User.EmailAddress);
                if (employee != null)
                {
                    employee.HashedPassword = BCrypt.HashPassword(command.ConfirmPassword);
                }

                return employee;
            }
        }
    }
}
