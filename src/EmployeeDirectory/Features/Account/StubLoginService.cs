﻿namespace EmployeeDirectory.Features.Account
{
    using Infrastructure;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;

    public class StubLoginService : ILoginService
    {
        public string AuthenticatedEmail { get; private set; }

        public Task LogIn(string email)
        {
            AuthenticatedEmail = email;
            return Task.CompletedTask;
        }

        public Task LogOut()
        {
            AuthenticatedEmail = null;
            return Task.CompletedTask;
        }

        public void Reset()
        {
            AuthenticatedEmail = null;
        }
    }
}
