﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using MediatR;

namespace EmployeeDirectory.Features.Account
{
    using System.Net.Mail;
    using Employee;
    using Infrastructure;
    using Microsoft.AspNetCore.Authorization;
    using Models;

    [AllowAnonymous]
    public class AccountController : BaseController
    {
        private readonly IMediator _mediator;
        private readonly ILoginService _loginService;
        private readonly UserContext _userContext;

        public AccountController(IMediator mediator, ILoginService loginService, UserContext userContext)
        {
            _mediator = mediator;
            _loginService = loginService;
            _userContext = userContext;
        }

        public IActionResult LogIn()
        {
            if (_userContext.IsAuthenticated)
            {
                return RedirectToAction("Index", "Employee");
            }

            return View();
        }

        [HttpPost]
        public async Task<ActionResult> LogIn(AccountLogin.Command command)
        {
            if (ModelState.IsValid)
            {
                await _mediator.Send(command);
                return RedirectToAction("Index", "Employee");
            }
      
           Message("error",ModelState[""].Errors.First().ErrorMessage);

           return View(); 
        }

        public async Task<ActionResult> LogOut()
        {
            await _loginService.LogOut();
            return RedirectToAction("LogIn", "Account");
        }

        public IActionResult ChangePassword()
        {
            return View();
        }

        [HttpPost]
        public async Task<ActionResult> ChangePassword(ChangePassword.Command command)
        {
            if (ModelState.IsValid)
            {
                await _mediator.Send(command);
                return RedirectToAction("Index", "Employee");
            }

            return View();
        }
    }
}