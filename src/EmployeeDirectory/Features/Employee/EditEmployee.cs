﻿namespace EmployeeDirectory.Features.Employee
{
    using System.ComponentModel.DataAnnotations;
    using Models;
    using System;
    using System.Linq;
    using MediatR;
    using AutoMapper;
    using FluentValidation;

    public class EditEmployee
    {
        public class Query : IRequest<ViewModel>
        {
            public Guid Id { get; set; }
        }

        public class ViewModel:IRequest
        {
            public Guid Id { get; set; }

            [Display(Name = "Email Address")]
            public string EmailAddress { get; set; }

            [Display(Name = "First Name")]
            public string FirstName { get; set; }

            [Display(Name = "Last Name")]
            public string LastName { get; set; }

            [Display(Name = "Job Title")]
            public string JobTitle { get; set; }

            [Display(Name = "Office")]
            public Office? Office { get; set; }

            public string PhoneNumber { get; set; }
        }

        public class Validator : AbstractValidator<ViewModel>
        {
            private readonly DirectoryContext _database;
            public Validator(DirectoryContext database)
            {
                _database = database;

                RuleFor(employee => employee.EmailAddress)
                    .Must(ValidateUniqueEmail).WithMessage("This email is already taken.")
                    .NotEmpty().WithMessage("Email should not be empty.")
                    .EmailAddress().WithMessage("A valid email is required.")
                    .Length(0, 255).WithMessage("Email should have 255 chars at most.");
                RuleFor(employee => employee.FirstName)
                    .NotEmpty().WithMessage("First name should not be empty.")
                    .Length(0, 255).WithMessage("First name should have 255 chars at most.");
                RuleFor(employee => employee.LastName)
                    .NotEmpty().WithMessage("Last name should not be empty.")
                    .Length(0, 255).WithMessage("Last name should have 255 chars at most.");
                RuleFor(employee => employee.JobTitle)
                    .NotEmpty().WithMessage("Job title should not be empty.")
                    .Length(0, 255).WithMessage("Job title should have 255 chars at most.");
                RuleFor(employee => employee.PhoneNumber).Length(0, 50).WithMessage("Phone number should have 50 chars at most.");
                RuleFor(employee => employee.Office).NotEmpty().WithMessage("Office should not be empty.");
            }

            private bool ValidateUniqueEmail(ViewModel employee, string email)
            {
                var result = _database.Employee.SingleOrDefault(x => x.EmailAddress == email);
                if (result != null && result.Id!=employee.Id)
                {
                    return false;
                }

                return true;
            }
        }

        public class QueryHandler:RequestHandler<Query,ViewModel>
        {
            private readonly DirectoryContext _database;
            private readonly IMapper _mapper;

            public QueryHandler(DirectoryContext database, IMapper mapper)
            {
                _database = database;
                _mapper = mapper;
            }

            protected override ViewModel HandleCore(Query query)
            {
                return _mapper.Map<ViewModel>(_database.Employee.Single(x => x.Id == query.Id));
            }
        }

        public class CommandHandler : RequestHandler<ViewModel>
        {
            private readonly DirectoryContext _database;
            private readonly IMapper _mapper;

            public CommandHandler(DirectoryContext database, IMapper mapper)
            {
                _database = database;
                _mapper = mapper;
            }

            protected override void HandleCore(ViewModel command)
            {
                // Get the existing entity
                var employee = _database.Employee.FirstOrDefault(x => x.Id == command.Id);
                _mapper.Map(command, employee);         
            }
        }
    }
}
