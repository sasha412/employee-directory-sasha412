﻿namespace EmployeeDirectory.Features.Employee
{
    using Microsoft.AspNetCore.Mvc;

    public abstract class BaseController : Controller
    {
        protected void Message(string type,string message)
        {
            TempData["ToastMessage"] = message;
            TempData["ToastType"] = type;
        }
    }
}