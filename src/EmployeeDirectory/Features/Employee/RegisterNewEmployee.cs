﻿namespace EmployeeDirectory.Features.Employee
{
    using System;
    using Models;
    using System.ComponentModel.DataAnnotations;
    using System.Linq;
    using System.Security.Cryptography.X509Certificates;
    using AutoMapper;
    using MediatR;
    using FluentValidation;
    using BCrypt.Net;

    public class RegisterNewEmployee
    {
        public class ViewModel: IRequest<Employee>
        {
            [Display(Name = "Email Address")]
            public string EmailAddress { get; set; }

            [Display(Name = "First Name")]
            public string FirstName { get; set; }

            [Display(Name = "Last Name")]
            public string LastName { get; set; }

            [Display(Name = "Password")]
            [DataType(DataType.Password)]
            public string Password { get; set; }

            [Display(Name = "Confirm Password")]
            [DataType(DataType.Password)]
            public string ConfirmPassword { get; set; }

            [Display(Name = "Job Title")]
            public string JobTitle { get; set; }

            [Display(Name = "Office")]
            public Office? Office { get; set; }

            public string PhoneNumber { get; set; }
        }

        public class Validator : AbstractValidator<ViewModel>
        {
            private readonly DirectoryContext _database;

            public Validator(DirectoryContext database)
            {
                _database = database;
                RuleFor(employee => employee.Password).NotEmpty().Length(5,16).WithMessage("Password should have atleast 6 chars.");
                RuleFor(employee => employee.ConfirmPassword).NotEmpty().Equal(employee=>employee.Password).WithMessage("Password and Confirm Password doesn't match.");
                RuleFor(employee => employee.EmailAddress).Must(ValidateUniqueEmail).WithMessage("This email is already taken.").NotEmpty().WithMessage("Email should not be empty.").EmailAddress().WithMessage("A valid email is required.").Length(0, 255).WithMessage("Email should have 255 chars at most.");
                RuleFor(employee => employee.FirstName).NotEmpty().WithMessage("First name should not be empty.").Length(0, 255).WithMessage("First name should have 255 chars at most.");
                RuleFor(employee => employee.LastName).NotEmpty().WithMessage("Last name should not be empty.").Length(0, 255).WithMessage("Last name should have 255 chars at most.");
                RuleFor(employee => employee.JobTitle).NotEmpty().WithMessage("Job title should not be empty.").Length(0, 255).WithMessage("Job title should have 255 chars at most.");
                RuleFor(employee => employee.PhoneNumber).Length(0, 50).WithMessage("Phone number should have 50 chars at most.");
                RuleFor(employee => employee.Office).NotEmpty().WithMessage("Office should not be empty.");
            }

            private bool ValidateUniqueEmail(string email)
            {
                var result = _database.Employee.SingleOrDefault(x => x.EmailAddress == email);
                return result == null;
            }
        }

        public class CommandHandler : RequestHandler<ViewModel,Employee>
        {
            private readonly IMapper _mapper;
            private readonly DirectoryContext _database;

            public CommandHandler(DirectoryContext database, IMapper mapper)
            {
                _database = database;
                _mapper = mapper;
            }
            protected override Employee HandleCore(ViewModel command)
            {                              
                var employee = _mapper.Map<Employee>(command);
                employee.HashedPassword= HashPassword(command.Password);
                _database.Add(employee);
                return employee;
            }

            private string HashPassword(string userPassword)
            {
                return  BCrypt.HashPassword(userPassword);              
            }            
        }
    }
}
