﻿namespace EmployeeDirectory.Features.Employee
{
    using Microsoft.AspNetCore.Mvc;
    using System.Threading.Tasks;
    using FluentValidation.Results;
    using Infrastructure;
    using MediatR;

    [CustomExceptionFilter]
    public class EmployeeController: BaseController
    {
        private readonly IMediator _mediator;
        private readonly ILoginService _loginService;

        public EmployeeController(IMediator mediator,ILoginService loginService)
        {
            _mediator = mediator;
            _loginService = loginService;
        }
   
        public async Task<ActionResult> Index(EmployeeIndex.Query query)
        {
            var model = await _mediator.Send(query);
            return View(model);
        }

        public ActionResult Register()
        {
            return View(new RegisterNewEmployee.ViewModel());
        }

        [HttpPost]
        public async Task<ActionResult> Register(RegisterNewEmployee.ViewModel command)
        {
            if (ModelState.IsValid)
            {
                await _mediator.Send(command);
                return RedirectToAction("Index");
            }

            return View();
        }

        public async Task<ActionResult> Edit(EditEmployee.Query query)
        {
            var model = await _mediator.Send(query);
            return View(model);
        }

        [HttpPost]
        public async Task<ActionResult> Edit(EditEmployee.ViewModel command)
        {
            if (ModelState.IsValid)
            {                
                await _mediator.Send(command);
                await _loginService.LogIn(command.EmailAddress);
                return RedirectToAction("Index");
            }
           
            return View();
        }

        public async Task<ActionResult> Delete(DeleteEmployee.Query query)
        {
            var model = await _mediator.Send(query);
            return PartialView("_Delete", model);
        }

        [HttpPost]
        public async Task<ActionResult> Delete(DeleteEmployee.Command command)
        {
            var model = await _mediator.Send(command);
            Message("success", $"{model.FirstName} {model.LastName} has been deleted");

            return Json(new { url = Url.Action("Index") });
        }
    }
}

