﻿namespace EmployeeDirectory.Features.Employee
{
    using System;
    using System.Linq;
    using MediatR;
    using Models;
    using AutoMapper;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;

    public class EmployeeIndex
    {
        public class Query: IRequest<ViewModel[]> { }
        public class ViewModel
        {
            public Guid Id { get; set; }
            public string EmailAddress { get; set; }
            public string FirstName { get; set; }
            public string LastName { get; set; }
            public string JobTitle { get; set; }
            public Office Office { get; set; }
            public string PhoneNumber { get; set; }
            public bool CanDelete { get; set; }
        }

        public class QueryHandler: RequestHandler<Query, ViewModel[]>
        {
            private readonly DirectoryContext _database;
            private readonly AutoMapper.IMapper _mapper;
            private readonly UserContext _userContext;

            public QueryHandler(DirectoryContext database, AutoMapper.IMapper mapper,UserContext userContext)
            {
                _database = database;
                _mapper = mapper;
                _userContext = userContext;
            }

            protected override ViewModel[] HandleCore(Query request)
            {
                var model = _database.Employee
                    .OrderBy(x => x.LastName)
                    .ThenBy(x => x.FirstName).ToArray()
                    .Select(_mapper.Map<ViewModel>).ToArray();
                foreach (var item in model)
                {
                    item.CanDelete = CheckPermission(item.Id);
                }
                return model;
            }

            private bool CheckPermission(Guid id)
            {
                if (_userContext.User!=null && _userContext.User.Id.Equals(id))
                {
                    return false;
                }
                var employeeRole = _database.EmployeeRoles.Where(x => x.Employee.Id == id).Select(x=>x.Role.Id);              
                var role = _database.Roles.Where(x => employeeRole.Contains(x.Id) && x.Name== "System Administrator");

                return !role.Any();
            }
        }      
    }
}
