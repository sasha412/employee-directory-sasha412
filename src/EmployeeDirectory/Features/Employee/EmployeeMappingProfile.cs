﻿namespace EmployeeDirectory.Features.Employee
{
    using AutoMapper;
    using Models;

    public class EmployeeMappingProfile:Profile
    {
        public EmployeeMappingProfile()
        {
            CreateMap<EditEmployee.ViewModel, Employee>()
                .ForMember(x => x.HashedPassword, options => options.Ignore());
            CreateMap<Employee, EditEmployee.ViewModel>();
            CreateMap<EmployeeIndex.ViewModel, Employee>()
                .ForMember(x => x.HashedPassword, options => options.Ignore());
            CreateMap<Employee, EmployeeIndex.ViewModel>()
                .ForMember(x => x.CanDelete, options => options.Ignore());
            CreateMap<RegisterNewEmployee.ViewModel, Employee>()
                .ForMember(x => x.Id, options => options.Ignore())
                .ForMember(x => x.HashedPassword, options => options.Ignore());
        }
    }
}
