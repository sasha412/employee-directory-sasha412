﻿namespace EmployeeDirectory.Features.Employee
{
    using System;
    using System.Linq;
    using AutoMapper;
    using MediatR;
    using Models;

    public class DeleteEmployee
    {
        public class Query : IRequest<ViewModel>
        {
            public Guid Id { get; set; }
        }

        public class Command : IRequest<ViewModel>
        {
            public Guid Id { get; set; }
        }

        public class ViewModel:IRequest
        {
            public Guid Id { get; set; }
            public string FirstName { get; set; }
            public string LastName { get; set; }
        }

        public class QueryHandler : RequestHandler<Query, ViewModel>
        {
            private readonly DirectoryContext _database;
            private readonly IMapper _mapper;

            public QueryHandler(DirectoryContext database, IMapper mapper)
            {
                _database = database;
                _mapper = mapper;
            }

            protected override ViewModel HandleCore(Query query)
            {
                var employee =_mapper.Map<ViewModel>(_database.Employee.Single(x => x.Id == query.Id));
                return employee;              
            }
        }

        public class CommandHandler : RequestHandler<Command, ViewModel>
        {
            private readonly IMapper _mapper;
            private readonly DirectoryContext _database;

            public CommandHandler(DirectoryContext database, IMapper mapper)
            {
                _database = database;
                _mapper = mapper;
            }
            protected override ViewModel HandleCore(Command command)
            {              
                var employee = _database.Employee.Single(x => x.Id == command.Id);
                _database.Remove(employee);

                var employeeRoles = _database.EmployeeRoles.Where(x => x.Employee.Id == command.Id);
                foreach (var item in employeeRoles)
                {
                    _database.Remove(item);
                }

                return _mapper.Map<ViewModel>(employee);
            }
        }
    }
}
