﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EmployeeDirectory.Models
{
    //Richly describe the current user, if any.
    public class UserContext
    {
        public Employee User { get; set; }
        public bool IsAuthenticated => User != null;
        public IEnumerable<Permission> Permissions { get; set; }  
    }
}
