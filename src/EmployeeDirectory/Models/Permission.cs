﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EmployeeDirectory.Models
{
    public enum Permission
    {
        Register = 1,
        Edit = 2,
        Delete = 3,
        Security = 4
    }
}
