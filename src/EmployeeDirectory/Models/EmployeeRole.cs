﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EmployeeDirectory.Models
{
    public class EmployeeRole: Entity
    {
        public virtual Employee Employee { get; set; }
        public virtual Role Role { get; set; }
    }
}
