﻿namespace EmployeeDirectory.Models
{
    public class Employee: Entity
    {
        public string EmailAddress { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string HashedPassword { get; set; }
        public string JobTitle { get; set; }
        public Office Office { get; set; }
        public string PhoneNumber { get; set; }
    }
}
