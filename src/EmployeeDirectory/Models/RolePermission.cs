﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EmployeeDirectory.Models
{
    public class RolePermission: Entity
    {
        public virtual Role Role { get; set; }
        public Permission Permission { get; set; }
    }
}
