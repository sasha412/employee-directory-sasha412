﻿$(document).ready(function() {
    $('.input-validation-error').addClass('is-invalid');
    $('.field-validation-error').addClass('text-danger');

    IncludeAntiForgeryTokenInAjaxPosts();
    SetRequiredAndMaxLength();
    OnDeleteIconClick();
    DisplayToast();
});

function IncludeAntiForgeryTokenInAjaxPosts() {
    $(document).ajaxSend(function (event, xhr, options) {
        if (options.type.toUpperCase() === "POST") {
            var token = $("#AntiForgeryToken input[name=__RequestVerificationToken]").val();
            xhr.setRequestHeader("RequestVerificationToken", token);
        }
    });
};

function SetRequiredAndMaxLength() {
    $('input, select ').each(function () {
        $(this).attr("maxlength", $(this).attr('data-val-length-max'));
        if ($(this).attr("data-val-required") !== undefined) {
            $(this).closest(".form-group")
                .children("label")
                .append('<span style="color:red;">*</span>');
        }
    });
}

function DisplayToast() {
    $("#toast").each(function() {
        var datatype = $(this).attr("data-type");
        var message = $(this).val();
        toastr[datatype](message);
    });
}

function OnConfirmButtonClick(url) {    
        $.ajax({
            type: "POST",
            url: url,
            success: function (result) {
                window.location.replace(result.url);
            },
            error: function (xhr, err) {
                toastr.error("An unexpected error occured with message: " + xhr.responseText + ". Please refresh.");
            }
    });
    return false;
}

function OnDeleteIconClick() {
    $(".employee-delete-icon").unbind().click(function (e) {
        e.preventDefault();
        $.ajax({
            type: "GET",
            url: $(this).attr('href'),
            success: function (result) {
                $("#modalPopup").html(result);
                $("#ConfirmModal").modal('toggle');
            },
            error: function (xhr, err) {
                toastr.error("An unexpected error occured with message: " + xhr.responseText + ". Please refresh.");
            }
        });
    });
}