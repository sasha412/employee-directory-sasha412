﻿namespace EmployeeDirectory.Infrastructure
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using System.Linq;
    using Microsoft.AspNetCore.Mvc;
    using Models;
    using Microsoft.AspNetCore.Mvc.Filters;
    using MediatR;
    using Microsoft.EntityFrameworkCore;

    public class UserContextFilter : IAsyncAuthorizationFilter
    {
        private readonly DirectoryContext _database;
        private readonly UserContext _userContext;
        private readonly ILoginService _loginService;

        public UserContextFilter(
            DirectoryContext database,
            UserContext userContext,
            ILoginService loginService)
        {
            _database = database;
            _userContext = userContext;
            _loginService = loginService;
        }

        public async Task OnAuthorizationAsync(AuthorizationFilterContext context)
        {
            var email = context.HttpContext.User.Identity.Name;
            var user = _database.Employee.SingleOrDefault(x => x.EmailAddress == email);

            if (email != null && user == null)
            {
                // The user is still authenticated under a previous email address,
                // but the email address in the database has since been updated.
                // Force them to log in again.
                await _loginService.LogOut();
                context.Result = new UnauthorizedResult();
            }
            else
            {
                if (user != null)
                {
                    var employeeRole = _database.EmployeeRoles.Where(x => x.Employee.Id == user.Id).Select(x => x.Role.Id);
                    if (employeeRole.Any())
                    {
                        var permissions = _database.RolePermissions
                            .Where(x => employeeRole.Contains(x.Role.Id))
                            .Select(x => x.Permission)
                            .Distinct();

                        _userContext.Permissions = permissions.ToList();
                    }
                }

                _userContext.User = user;                
            }
        }
    }
}