﻿namespace EmployeeDirectory.Infrastructure
{
    using Microsoft.AspNetCore.Mvc.Rendering;
    using System;
    using System.ComponentModel.DataAnnotations;
    using System.Linq.Expressions;

    public static class HtmlHelperExtensions
    {
        private static bool IsEnum<T>()
        {
            return typeof(T).IsEnum || Nullable.GetUnderlyingType(typeof(T))?.IsEnum == true;
        }

        private static bool IsRequired(MemberExpression memberExpression)
        {
            return memberExpression.Member.IsDefined(typeof(RequiredAttribute), true);
        }

        public static TagBuilder Input<TModel, TValue>(this IHtmlHelper<TModel> html, Expression<Func<TModel, TValue>> expression)
        {           
            var div = new TagBuilder("div");
            var memberExpression = (MemberExpression)expression.Body;  
            div.AddCssClass("form-group");
            div.InnerHtml.AppendHtml(html.LabelFor(expression));

            if (IsEnum<TValue>())
            {
                div.InnerHtml.AppendHtml(html.DropDownListFor(expression, html.GetEnumSelectList(typeof(TValue)), "--Select--",new { @class="form-control" }));
            }
            else
                div.InnerHtml.AppendHtml(html.EditorFor(expression, new { htmlAttributes = new { @class = "form-control" } }));

            div.InnerHtml.AppendHtml(html.ValidationMessageFor(expression));

            return div;
        }

        public static TagBuilder Button<TModel>(this IHtmlHelper<TModel> html, string text, string url, string buttonClass= "btn btn-primary", string onClick="", string customClass="")
        {
            var button = new TagBuilder("a");
            button.MergeAttribute("href", url);
            button.MergeAttribute("role", "button");
            if(!String.IsNullOrEmpty(onClick))
                button.MergeAttribute("onclick",onClick);
            button.AddCssClass(customClass);
            button.AddCssClass(buttonClass);
            button.InnerHtml.Append(text);

            return button;
        }

        public static TagBuilder GoogleDesignIcon(this IHtmlHelper html, string iconType, string url, string cssClass="")
        {
            var button = new TagBuilder("a");
            button.MergeAttribute("href", url);
            button.MergeAttribute("class",cssClass);
            var icon = new TagBuilder("i");
            icon.AddCssClass("material-icons text-primary");
            icon.InnerHtml.Append(iconType);
            button.InnerHtml.AppendHtml(icon);

            return button;
        }

        public static TagBuilder ConfirmButton<TModel>(this IHtmlHelper<TModel> html, string text, string url,
            string buttonClass="", string onClick="", string customClass="")
        {
            return html.Button(text, url, buttonClass, onClick, customClass);
        }

        public static TagBuilder CancelButton<TModel>(this IHtmlHelper<TModel> html, string url)
        { 
            return html.Button("Cancel", url,"btn btn-secondary","cancel");
        }

        public static TagBuilder SaveButton<TModel>(this IHtmlHelper<TModel> html)
        {
            var button = new TagBuilder("button");
            button.MergeAttribute("type", "submit");
            button.InnerHtml.Append("Save");
            button.AddCssClass("btn btn-primary");
            return button;
        }
    }
}