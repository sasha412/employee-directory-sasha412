﻿namespace EmployeeDirectory.Tests.Features.Employee
{
    using System;
    using Models;
    using System.Linq;
    using System.Threading.Tasks;
    using EmployeeDirectory.Features.Employee;
    using static Testing;

    class EditEmployeeTests
    {
        public async Task ShouldUpdateExisitingEmployee()
        {
           var dustin= await Send(new RegisterNewEmployee.ViewModel()
           {
                FirstName = "Dustin",
                LastName = "Wells",
                JobTitle = "President & CEO",
                Office = Office.Austin,
                EmailAddress = SampleEmail(),
                PhoneNumber = "555-123-0001",
                Password = "password",
                ConfirmPassword = "password"
           });

            var editDustin = new EditEmployee.ViewModel()
            {
                Id = dustin.Id,
                FirstName = "Dustin",
                LastName = "Wells",
                JobTitle = "President & CEO",
                Office = Office.Austin,
                EmailAddress = dustin.EmailAddress,
                PhoneNumber = "555-123-0001"
            };
            await Send(editDustin);

            var command = new EditEmployee.ViewModel()
            {
                Id = dustin.Id,
                EmailAddress = SampleEmail(),
                FirstName = "Destin",
                LastName = "Walls",
                JobTitle = "President",
                Office = Office.Austin,
                PhoneNumber = "555-908-1234"
            };

            await Send(command);           
            Employee employee = Query(database => database.Employee.Single(x=>x.Id==dustin.Id));

            employee.ShouldMatch(new Employee()
            {
                Id=dustin.Id,
                EmailAddress = command.EmailAddress,
                FirstName = "Destin",
                LastName = "Walls",
                JobTitle = "President",
                Office = Office.Austin,
                PhoneNumber = "555-908-1234",
                HashedPassword = employee.HashedPassword
            });

            new EditEmployee.ViewModel()
            {
                Id = dustin.Id,
                EmailAddress = command.EmailAddress,
                FirstName = "Destin",
                LastName = "Walls",
                JobTitle = "President",
                Office = Office.Austin,
                PhoneNumber = "555-908-1234",
            }.ShouldValidate();

            new RegisterNewEmployee.ViewModel()
            {
                FirstName = "Dustin",
                LastName = "Wells",
                JobTitle = "President & CEO",
                Office = Office.Austin,
                EmailAddress = command.EmailAddress,
                PhoneNumber = "555-123-0001",
                Password = "password",
                ConfirmPassword = "password"
            }.ShouldNotValidate("This email is already taken.");
        }

        public async Task ShouldGetEmloyeeById()
        {
            var email = SampleEmail();
            var dustin = await Send(new RegisterNewEmployee.ViewModel()
            {
                FirstName = "Dustin",
                LastName = "Wells",
                JobTitle = "President & CEO",
                Office = Office.Austin,
                EmailAddress = email,
                PhoneNumber = "555-123-0001",
                Password = "password",
                ConfirmPassword = "password"
            });

            var query = new EditEmployee.Query { Id = dustin.Id };
            var result= await Send(query);

            result.ShouldMatch(new EditEmployee.ViewModel()
            {
                Id=dustin.Id,
                EmailAddress = email,
                FirstName = "Dustin",
                LastName = "Wells",
                JobTitle = "President & CEO",
                Office = Office.Austin,
                PhoneNumber = "555-123-0001"
            });
        }

        public void ShouldRequireMaxFields()
        {
            new EditEmployee.ViewModel()
            {
                EmailAddress = "85MxtyqJRhcykMT0LTVAUNZ0zM5zUsKv8hH7d238IigvHeQu9cO150mUXH3X4U19RIndZY4KPd1ZaaUhUxvKP0Ear94Mlp3dQVAUhg8rWutorHizSdYF1qlvtkDR0l9nkxZ56LJJzId6RoMupjN72LPTbMe3rHHRjRO5NLpMbURyLRRCSGhxvmiAxmhTC8UTYIImIrumht6YcaS0RxWePE1ZXuVzjFreAqT5yg9Cv6RadYUKOLtuYmX1X73BrIjH@gmail.com",
                FirstName = "85MxtyqJRhcykMT0LTVAUNZ0zM5zUsKv8hH7d238IigvHeQu9cO150mUXH3X4U19RIndZY4KPd1ZaaUhUxvKP0Ear94Mlp3dQVAUhg8rWutorHizSdYF1qlvtkDR0l9nkxZ56LJJzId6RoMupjN72LPTbMe3rHHRjRO5NLpMbURyLRRCSGhxvmiAxmhTC8UTYIImIrumht6YcaS0RxWePE1ZXuVzjFreAqT5yg9Cv6RadYUKOLtuYmX1X73BrIjH",
                LastName = "XS7fNMr4tm86AFCr952wl2H1mCv8ywPZnFBMulNchz7nWNpgFqAXXRQ5m8x6PxKxMdUMUdYCd1oPzBFjbuLfDStHrCMMe4jVL87PUFIVc9ukW9lsWNWk9dklixDBIzdRtmOHEy9OrOLfFUS3WmYhZTWD5zFcuhGcb6HVRStEcTIlGe1Rb3OyX5X1udxucGkJdX4TC71N1csafMjGWNXuQdZV5v1QIWf5HWnJZc1nZtjhFmhx6RQB98UJ2DtGYmQY",
                JobTitle = "9XmEguS94HBdINxFAKuBmZPGdLNpd6iTTbUtnMbGmNHBcA4bMxCY1VAA2ayE93FK9a4CBNIwv1U1ZfCiHnECCZXce1OPTscF9kWhvFazwqGHyNQgLoNMm7di0ZP2ZUMIbwBgWNvGxs1kyIo4Q9obDs21dvYtHSyH6rBZFVhT3Ase9afHnI6xn7HIzfHREodrbgd3uxA3EwFxh7yy3Dcr3Lh7qQrqQSX0hjlrPLPawocqjFzGmRa6dHtzvuCsScqD",
                PhoneNumber = "v3xur1ZkWgv8Jxr748W1wkXsI3mv0DcCOfBi41HSoSMDXF5BJKm",
                Office = Office.Austin
            }.ShouldNotValidate("Email should have 255 chars at most.",
                "First name should have 255 chars at most.",
                "Last name should have 255 chars at most.",
                "Job title should have 255 chars at most.",
                "Phone number should have 50 chars at most.");
        }

        public void ShouldRequireValidEmail()
        {
            new EditEmployee.ViewModel()
            {
                FirstName = "Sashank",
                LastName = "Bandhakavi",
                JobTitle = "Consultant",
                Office = Office.Austin
            }.ShouldNotValidate("Email should not be empty.");

            new EditEmployee.ViewModel()
            {
                FirstName = "Sashank",
                LastName = "Bandhakavi",
                JobTitle = "Consultant",
                Office = Office.Austin,
                EmailAddress = "abc.b!ca.ecr@gmail.com"
            }.ShouldValidate();

            new EditEmployee.ViewModel()
            {
                FirstName = "Sashank",
                LastName = "Bandhakavi",
                JobTitle = "Consultant",
                Office = Office.Austin,
                EmailAddress = "@gmail.com"
            }.ShouldNotValidate("A valid email is required.");

            new EditEmployee.ViewModel()
            {
                FirstName = "Sashank",
                LastName = "Bandhakavi",
                JobTitle = "Consultant",
                Office = Office.Austin,
                EmailAddress = "abc.bca@gmail.com"
            }.ShouldValidate();
        }

        public void ShouldRequireMinimumFields()
        {
            new EditEmployee.ViewModel().ShouldNotValidate(
                "Email should not be empty.",
                "First name should not be empty.",
                "Last name should not be empty.",
                "Job title should not be empty.",
                "Office should not be empty.");
        }
    }
}
