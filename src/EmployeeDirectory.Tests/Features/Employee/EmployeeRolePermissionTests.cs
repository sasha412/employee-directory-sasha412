﻿namespace EmployeeDirectory.Tests.Features.Employee
{
    using Models;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using Should;
    using static Testing;

    class EmployeeRolePermissionTests
    {
        public void ShouldVerifyRolePermissionsTable()
        {
            var role = new Role()
            {
                Id = Guid.NewGuid(),
                Name = "admin"
            };

            var rolePermission = new  RolePermission()
            {
                Id = Guid.NewGuid(),
                Role = role,
                Permission = Permission.Edit                
            };

            Transaction(database =>
            {
                database.RolePermissions.Add(rolePermission);
            });

            Transaction(database =>
            {
                database.RolePermissions.SingleOrDefault(x => x.Role.Id == role.Id && x.Permission == rolePermission.Permission).Permission.ShouldEqual(Permission.Edit);
                database.RolePermissions.SingleOrDefault(x => x.Role.Id == role.Id && x.Permission == rolePermission.Permission).Role.Name.ShouldEqual(role.Name);
                database.RolePermissions.SingleOrDefault(x => x.Role.Id == role.Id && x.Permission == rolePermission.Permission).Role.Id.ShouldEqual(role.Id);
            });
        }
    }
}
