﻿namespace EmployeeDirectory.Tests.Features.Employee
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using EmployeeDirectory.Features.Employee;
    using Models;
    using Should;
    using static Testing;

    class DeleteEmployeeTests
    {
        public async Task ShouldDeleteExisitingEmployee()
        {
            var dustin = new Employee
            {
                Id=Guid.NewGuid(),
                FirstName = "Dustin",
                LastName = "Wells",
                JobTitle = "President & CEO",
                Office = Office.Austin,
                EmailAddress = SampleEmail(),
                PhoneNumber = "555-123-0001",
                HashedPassword = "hashedpassword"
            };
      
            var role = new Role()
            {
                Id = Guid.NewGuid(),
                Name = "admin"
            };

            var employeeRole = new EmployeeRole()
            {
                Id = Guid.NewGuid(),
                Employee = dustin,
                Role = role
            };

            Transaction(database =>
            {
                database.EmployeeRoles.Add(employeeRole);
            });

            var command = new DeleteEmployee.Command
            {
                Id = dustin.Id               
            };

            await Send(command);

            IEnumerable<Employee> deletedEmployeeList = Query(database=>database.Employee.Where(x => x.Id == dustin.Id).ToList());          
            deletedEmployeeList.Count().ShouldEqual(0);         
        }

        public async Task ShouldGetEmloyeeById()
        {
            var dustin = await Send(new RegisterNewEmployee.ViewModel()
            {
                FirstName = "Dustin",
                LastName = "Wells",
                JobTitle = "President & CEO",
                Office = Office.Austin,
                EmailAddress = SampleEmail(),
                PhoneNumber = "555-123-0001",
                Password = "password",
                ConfirmPassword = "password"
            });

            var query = new DeleteEmployee.Query { Id = dustin.Id};
            var result = await Send(query);

            result.ShouldMatch(new DeleteEmployee.ViewModel
            {
                Id = dustin.Id,              
                FirstName = "Dustin",
                LastName = "Wells"
            });
        }
    }
}
