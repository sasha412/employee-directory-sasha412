﻿namespace EmployeeDirectory.Tests.Features.Employee
{
    using Models;
    using static Testing;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using EmployeeDirectory.Features.Employee;
    using Should;

    class EmployeeRoleTests
    {
        public void ShouldVerifyEmployeeRolesTable()
        {
            var employee = new Employee()
            {
                Id=Guid.NewGuid(),
                EmailAddress = SampleEmail(),
                FirstName = "Sashank",
                LastName = "Bandhakavi",
                JobTitle = "Junior Consultant",
                Office = Office.Austin,
                PhoneNumber = "512-555-5678",
                HashedPassword = "password"
            };

            var role = new Role()
            {
                Id = Guid.NewGuid(),
                Name = "admin"
            };
           
            var employeeRole = new EmployeeRole()
            {
                Id = Guid.NewGuid(),
                Employee = employee,
                Role = role
            };

            Transaction(database =>
            {
                database.EmployeeRoles.Add(employeeRole);                             
            });

            Transaction(database =>
            {
                database.EmployeeRoles.SingleOrDefault(x => x.Role.Id == role.Id && x.Employee.Id == employee.Id).Employee.ShouldMatch(employee);
                database.EmployeeRoles.SingleOrDefault(x => x.Role.Id == role.Id && x.Employee.Id == employee.Id).Role.ShouldMatch(role);
            });
        }
    }
}
