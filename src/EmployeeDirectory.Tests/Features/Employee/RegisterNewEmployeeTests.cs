﻿namespace EmployeeDirectory.Tests.Features.Employee
{
    using System.Linq;
    using System.Threading.Tasks;
    using EmployeeDirectory.Features.Employee;
    using Models;
    using static Testing;
    using BCrypt.Net;
    using Should;

    class RegisterNewEmployeeTests
    {
        public async Task ShouldCreateNewEmployee()
        {
            var command = new RegisterNewEmployee.ViewModel()
            {
                EmailAddress = SampleEmail(),
                FirstName = "Sashank",
                LastName = "Bandhakavi",
                JobTitle = "Junior Consultant",
                Office = Office.Austin,
                PhoneNumber = "512-555-5678",
                Password="password",
                ConfirmPassword = "password"
            };

            var result= await Send(command);
            Employee employee =  Query(database=> database.Employee.Single(x=>x.Id==result.Id));
            employee.ShouldMatch(new Employee
            {
                Id=result.Id,
                EmailAddress= command.EmailAddress,
                FirstName="Sashank",
                LastName ="Bandhakavi",
                JobTitle = "Junior Consultant",
                Office = Office.Austin,
                PhoneNumber = "512-555-5678",
                HashedPassword = result.HashedPassword
            });

            BCrypt.Verify(command.Password, employee.HashedPassword).ShouldBeTrue();

            new RegisterNewEmployee.ViewModel()
            {
                EmailAddress = command.EmailAddress,
                FirstName = "Sashank",
                LastName = "Bandhakavi",
                JobTitle = "Consultant",
                Office = Office.Austin,
                Password = "password",
                ConfirmPassword = "password"
            }.ShouldNotValidate("This email is already taken.");
        }

        public void ShouldRequireMinimumFields()
        {
            new RegisterNewEmployee.ViewModel().ShouldNotValidate(
                "'Confirm Password' should not be empty.",
                "Email should not be empty.",
                "First name should not be empty.",
                "Job title should not be empty.",
                "Last name should not be empty.",
                "Office should not be empty.",
                "'Password' should not be empty.");
        }

        public void ShouldRequireMaxFields()
        {
            new RegisterNewEmployee.ViewModel()
            {
                EmailAddress = "85MxtyqJRhcykMT0LTVAUNZ0zM5zUsKv8hH7d238IigvHeQu9cO150mUXH3X4U19RIndZY4KPd1ZaaUhUxvKP0Ear94Mlp3dQVAUhg8rWutorHizSdYF1qlvtkDR0l9nkxZ56LJJzId6RoMupjN72LPTbMe3rHHRjRO5NLpMbURyLRRCSGhxvmiAxmhTC8UTYIImIrumht6YcaS0RxWePE1ZXuVzjFreAqT5yg9Cv6RadYUKOLtuYmX1X73BrIjH@gmail.com",
                FirstName = "85MxtyqJRhcykMT0LTVAUNZ0zM5zUsKv8hH7d238IigvHeQu9cO150mUXH3X4U19RIndZY4KPd1ZaaUhUxvKP0Ear94Mlp3dQVAUhg8rWutorHizSdYF1qlvtkDR0l9nkxZ56LJJzId6RoMupjN72LPTbMe3rHHRjRO5NLpMbURyLRRCSGhxvmiAxmhTC8UTYIImIrumht6YcaS0RxWePE1ZXuVzjFreAqT5yg9Cv6RadYUKOLtuYmX1X73BrIjH",
                LastName = "XS7fNMr4tm86AFCr952wl2H1mCv8ywPZnFBMulNchz7nWNpgFqAXXRQ5m8x6PxKxMdUMUdYCd1oPzBFjbuLfDStHrCMMe4jVL87PUFIVc9ukW9lsWNWk9dklixDBIzdRtmOHEy9OrOLfFUS3WmYhZTWD5zFcuhGcb6HVRStEcTIlGe1Rb3OyX5X1udxucGkJdX4TC71N1csafMjGWNXuQdZV5v1QIWf5HWnJZc1nZtjhFmhx6RQB98UJ2DtGYmQY",
                JobTitle = "9XmEguS94HBdINxFAKuBmZPGdLNpd6iTTbUtnMbGmNHBcA4bMxCY1VAA2ayE93FK9a4CBNIwv1U1ZfCiHnECCZXce1OPTscF9kWhvFazwqGHyNQgLoNMm7di0ZP2ZUMIbwBgWNvGxs1kyIo4Q9obDs21dvYtHSyH6rBZFVhT3Ase9afHnI6xn7HIzfHREodrbgd3uxA3EwFxh7yy3Dcr3Lh7qQrqQSX0hjlrPLPawocqjFzGmRa6dHtzvuCsScqD",
                PhoneNumber= "v3xur1ZkWgv8Jxr748W1wkXsI3mv0DcCOfBi41HSoSMDXF5BJKm",
                Office = Office.Austin,
                Password = "password",
                ConfirmPassword = "password"
            }.ShouldNotValidate("Email should have 255 chars at most.",
                "First name should have 255 chars at most.",
                "Last name should have 255 chars at most.",
                "Job title should have 255 chars at most.",
                "Phone number should have 50 chars at most.");
        }

        public void ShouldRequireValidEmail()
        {
            new RegisterNewEmployee.ViewModel()
            {
                FirstName = "Sashank",
                LastName="Bandhakavi",
                JobTitle = "Consultant",
                Office= Office.Austin,
                Password = "password",
                ConfirmPassword = "password"
            }.ShouldNotValidate("Email should not be empty.");

            new RegisterNewEmployee.ViewModel()
            {
                FirstName = "Sashank",
                LastName = "Bandhakavi",
                JobTitle = "Consultant",
                Office = Office.Austin,
                Password = "password",
                ConfirmPassword = "password",
                EmailAddress = "abc.b!ca.ecr@gmail.com"
            }.ShouldValidate();

            new RegisterNewEmployee.ViewModel()
            {
                FirstName = "Sashank",
                LastName = "Bandhakavi",
                JobTitle = "Consultant",
                Office = Office.Austin,
                EmailAddress = "@gmail.com",
                Password = "password",
                ConfirmPassword = "password"
            }.ShouldNotValidate("A valid email is required.");

            new RegisterNewEmployee.ViewModel()
            {
                FirstName = "Sashank",
                LastName = "Bandhakavi",
                JobTitle = "Consultant",
                Office = Office.Austin,
                EmailAddress = "abc.bca@gmail.com",
                Password = "password",
                ConfirmPassword = "password"
            }.ShouldValidate();
        }
    }
}
