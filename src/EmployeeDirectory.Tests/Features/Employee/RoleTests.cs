﻿namespace EmployeeDirectory.Tests.Features.Employee
{
    using EmployeeDirectory.Features.Employee;
    using Models;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using Should;
    using static Testing;

    class RoleTests
    {
        public void ShouldVerifyRoleTable()
        {
            var role = new Role()
            {
                Id = Guid.NewGuid(),
                Name = "admin"
            };

            Transaction(database =>
            {                
                database.Roles.Add(role);
            });

          var dbRole =  Query<Role>( database => database.Set<Role>().SingleOrDefault(x=>x.Id==role.Id));
          dbRole.Name.ShouldEqual("admin");
          dbRole.Id.ShouldEqual(role.Id);
        }  
    }
}
