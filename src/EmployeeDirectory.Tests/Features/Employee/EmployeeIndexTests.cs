﻿namespace EmployeeDirectory.Tests.Features.Employee
{
    using System;
    using System.Data;
    using System.Linq;
    using System.Threading.Tasks;
    using EmployeeDirectory.Features.Employee;
    using Should;
    using Models;

    using static Testing;

    public class EmployeeIndexTests
    {
        public async Task ShouldSortEmployees()
        {
            var jimmy = await Send(new RegisterNewEmployee.ViewModel()
            { 
                FirstName = "Jimmy",
                LastName = "Bogard",
                JobTitle = "Chief Architect",
                Office = Office.Austin,
                EmailAddress = SampleEmail(),
                PhoneNumber = "555-123-0003",
                Password = "password",
                ConfirmPassword = "password"
            });

            var glenn = await Send(new RegisterNewEmployee.ViewModel()
            {
                FirstName = "Timmy",
                LastName = "Bogard",
                JobTitle = "Executive Vice President, Operations and Strategy",
                Office = Office.Austin,
                EmailAddress = SampleEmail(),
                PhoneNumber = "555-123-0004",
                Password = "password",
                ConfirmPassword = "password"
            });

            var kathy = await Send(new RegisterNewEmployee.ViewModel()
            {   
                FirstName = "Kathy",
                LastName = "Chesner",
                JobTitle = "Director of Finance",
                Office = Office.Austin,
                EmailAddress = SampleEmail(),
                PhoneNumber = "555-123-0006",
                Password = "password",
                ConfirmPassword = "password"
            });

            var expectedIds = new[] { jimmy.Id, glenn.Id, kathy.Id };

            var query = new EmployeeIndex.Query();
            var result = await Send(query);          
            result = result.Where(x => expectedIds.Contains(x.Id)).ToArray();
            result.ShouldMatch(
                new EmployeeIndex.ViewModel
                {
                    Id=jimmy.Id,
                    FirstName = "Jimmy",
                    LastName = "Bogard",
                    JobTitle = "Chief Architect",
                    Office = Office.Austin,
                    EmailAddress = jimmy.EmailAddress,
                    PhoneNumber = "555-123-0003",
                    CanDelete = true
                },
                new EmployeeIndex.ViewModel
                {
                    Id=glenn.Id,
                    FirstName = "Timmy",
                    LastName = "Bogard",
                    JobTitle = "Executive Vice President, Operations and Strategy",
                    Office = Office.Austin,
                    EmailAddress = glenn.EmailAddress,
                    PhoneNumber = "555-123-0004",
                    CanDelete = true
                },
                new EmployeeIndex.ViewModel
                {
                    Id=kathy.Id,
                    FirstName = "Kathy",
                    LastName = "Chesner",
                    JobTitle = "Director of Finance",
                    Office = Office.Austin,
                    EmailAddress = kathy.EmailAddress,
                    PhoneNumber = "555-123-0006",
                    CanDelete = true
                });
        }

        public async Task ShouldAssignEmployeeProperties()
        {
            var sashank =   await Send(new RegisterNewEmployee.ViewModel()
            {
                EmailAddress = SampleEmail(),
                FirstName = "Sashank",
                LastName = "Bandhakavi",
                JobTitle = "Junior Consultant",
                Office = Office.Austin,
                PhoneNumber = "512-555-5678",
                Password = "password",
                ConfirmPassword = "password"
            });

            var expectedIds = new[] { sashank.Id };
            var query = new EmployeeIndex.Query();
            var result = await Send(query);

            result = result.Where(x => expectedIds.Contains(x.Id)).ToArray();
            result.Length.ShouldEqual(1);
            result.ShouldMatch(
                new EmployeeIndex.ViewModel()
                {
                    Id=sashank.Id,
                    EmailAddress = sashank.EmailAddress,
                    FirstName = "Sashank",
                    LastName = "Bandhakavi",
                    JobTitle = "Junior Consultant",
                    Office = Office.Austin,
                    PhoneNumber = "512-555-5678",
                    CanDelete = true
                });
        }
    }
}
