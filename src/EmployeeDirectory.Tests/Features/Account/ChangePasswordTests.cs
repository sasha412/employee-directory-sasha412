﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EmployeeDirectory.Tests.Features.Account
{
    using EmployeeDirectory.Features.Account;
    using EmployeeDirectory.Features.Employee;
    using Models;
    using static Testing;
    using MediatR;
    using BCrypt.Net;
    using Should;

    class ChangePasswordTests
    {
        public async Task ShouldVerifyChangePassword()
        {
            var employee = LogIn().Result;
            var password = "12345678";
            await Send(new ChangePassword.Command()
            {
                CurrentPassword = "password",
                NewPassword = password,
                ConfirmPassword = password
            });

            var updatedEmployee = Query(database => database.Employee.SingleOrDefault(x => x.Id == employee.Id));
            BCrypt.Verify(password, updatedEmployee.HashedPassword).ShouldEqual(true);
        }

        public void ShouldVerifyValidChangePassword()
        {
            var employee = LogIn().Result;

            var password = "12345678";
            new ChangePassword.Command()
            {
                CurrentPassword = "password",
                NewPassword = password,
                ConfirmPassword = password
            }.ShouldValidate();

            password = "123";
            new ChangePassword.Command()
            {
                CurrentPassword = "password",
                NewPassword = password,
                ConfirmPassword = password
            }.ShouldNotValidate("Invalid new password.");
        }
    }
}
