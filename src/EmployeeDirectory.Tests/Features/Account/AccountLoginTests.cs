﻿namespace EmployeeDirectory.Tests.Features.Account
{
    using System;
    using System.Linq;
    using EmployeeDirectory.Features.Account;
    using EmployeeDirectory.Features.Employee;
    using Microsoft.CodeAnalysis.CSharp;
    using Models;
    using static Testing;
    using MediatR;
    using System.Threading.Tasks;

    class AccountLoginTests
    {
        public async Task ShouldVerifyLogin()
        {
            var dustin = await Send(new RegisterNewEmployee.ViewModel()
            {
                FirstName = "Dustin",
                LastName = "Wells",
                JobTitle = "President & CEO",
                Office = Office.Austin,
                EmailAddress = SampleEmail(),
                PhoneNumber = "555-123-0001",
                Password = "password",
                ConfirmPassword = "password"
            });

            new AccountLogin.Command()
            {
                EmailAddress = "",
                Password = ""
            }.ShouldNotValidate("Incorrect Email and/or Password");

            new AccountLogin.Command()
            {
                EmailAddress = dustin.EmailAddress,
                Password = String.Empty
            }.ShouldNotValidate("Incorrect Email and/or Password");

            new AccountLogin.Command()
            {
                EmailAddress = dustin.EmailAddress,
                Password = null
            }.ShouldNotValidate("Incorrect Email and/or Password");

            new AccountLogin.Command()
            {
                EmailAddress = null,
                Password = "password"
            }.ShouldNotValidate("Incorrect Email and/or Password");

            new AccountLogin.Command()
            {
                EmailAddress = null,
                Password = null
            }.ShouldNotValidate("Incorrect Email and/or Password");

            new AccountLogin.Command()
            {
                EmailAddress = "",
                Password = "password"
            }.ShouldNotValidate("Incorrect Email and/or Password");

            new AccountLogin.Command()
            {
                EmailAddress = "sashank@example.com",
                Password = "password"
            }.ShouldNotValidate("Incorrect Email and/or Password");

            new AccountLogin.Command()
            {
                EmailAddress = dustin.EmailAddress,
                Password = "password4567"
            }.ShouldNotValidate("Incorrect Email and/or Password");

            new AccountLogin.Command()
            {
                EmailAddress = dustin.EmailAddress,
                Password = "password"
            }.ShouldValidate();
        }

        public void ShouldValidateHandler()
        {
            var logIn = LogIn().Result;
            var employee = Query(database => database.Employee.SingleOrDefault(x => x.EmailAddress == logIn.EmailAddress));

            employee.ShouldMatch(new Employee(){
                FirstName = "Dustin",
                LastName = "Wells",
                JobTitle = "President & CEO",
                Office = Office.Austin,
                EmailAddress = logIn.EmailAddress,
                PhoneNumber = "555-123-0001",
                HashedPassword = logIn.HashedPassword,
                Id= logIn.Id
            });
        }
    }
}
