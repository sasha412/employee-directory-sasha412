﻿namespace EmployeeDirectory.Tests
{
    using System;
    using System.IO;
    using System.Linq;
    using System.Threading.Tasks;
    using EmployeeDirectory.Features.Account;
    using EmployeeDirectory.Features.Employee;
    using MediatR;
    using Microsoft.Extensions.Configuration;
    using Microsoft.Extensions.DependencyInjection;
    using Models;
    using Newtonsoft.Json;
    using FluentValidation;
    using FluentValidation.Results;
    using Infrastructure;

    public static class Testing
    {
        private static readonly IServiceScopeFactory ScopeFactory;

        public static IConfigurationRoot Configuration { get; }

        static Testing()
        {
            Configuration = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json", true, true)
                .AddEnvironmentVariables(Program.ApplicationName + ":")
                .Build();

            var startup = new Startup(Configuration);
            var services = new ServiceCollection();
            startup.ConfigureServices(services);
            services.AddSingleton<ILoginService, StubLoginService>();

            var rootContainer = services.BuildServiceProvider();
            ScopeFactory = rootContainer.GetService<IServiceScopeFactory>();
        }

        public static string Json(object value)
        {
            return JsonConvert.SerializeObject(value, Formatting.Indented);
        }

        public static async Task Send(IRequest message)
        {
            using (var scope = ScopeFactory.CreateScope())
            {
                var serviceProvider = scope.ServiceProvider;

                var database = serviceProvider.GetService<DirectoryContext>();

                try
                {
                    database.BeginTransaction();
                    EmulateUserContextFilter(serviceProvider, database);
                    Validator(serviceProvider, message)?.Validate(message).ShouldBeSuccessful();
                    await serviceProvider.GetService<IMediator>().Send(message);
                    database.CloseTransaction();
                }
                catch (Exception exception)
                {
                    database.CloseTransaction(exception);
                    throw;
                }
            }
        }

        public static async Task<TResponse> Send<TResponse>(IRequest<TResponse> message)
        {
            TResponse response;

            using (var scope = ScopeFactory.CreateScope())
            {
                var serviceProvider = scope.ServiceProvider;

                var database = serviceProvider.GetService<DirectoryContext>();

                try
                {
                    database.BeginTransaction();
                    EmulateUserContextFilter(serviceProvider, database);
                    Validator(serviceProvider, message)?.Validate(message).ShouldBeSuccessful();
                    response = await serviceProvider.GetService<IMediator>().Send(message);
                    database.CloseTransaction();
                }
                catch (Exception exception)
                {
                    database.CloseTransaction(exception);
                    throw;
                }
            }

            return response;
        }
        public static void Transaction(Action<DirectoryContext> action)
        {
            using (var scope = ScopeFactory.CreateScope())
            {
                var database = scope.ServiceProvider.GetService<DirectoryContext>();

                try
                {
                    database.BeginTransaction();
                    action(database);
                    database.CloseTransaction();
                }
                catch (Exception exception)
                {
                    database.CloseTransaction(exception);
                    throw;
                }
            }
        }

        public static TResult Query<TResult>(Func<DirectoryContext, TResult> query)
        {
            var result = default(TResult);

            Transaction(database =>
            {
                result = query(database);
            });

            return result;
        }

        public static TEntity Query<TEntity>(Guid id) where TEntity : Entity
        {
            return Query(database => database.Set<TEntity>().Find(id));
        }

        public static int Count<TEntity>() where TEntity : class
        {
            return Query(database => database.Set<TEntity>().Count());
        }

        public static T DeepCopy<T>(T value)
        {
            return JsonConvert.DeserializeObject<T>(Json(value));
        }

        private static void EmulateUserContextFilter(IServiceProvider serviceProvider, DirectoryContext database)
        {
            var loginService = (StubLoginService)serviceProvider.GetService<ILoginService>();
            if (loginService.AuthenticatedEmail != null)
            {
                var userContext = serviceProvider.GetService<UserContext>();
                var user = database.Employee.SingleOrDefault(x => x.EmailAddress == loginService.AuthenticatedEmail);
                userContext.User = user;
            }
        }

        public static ValidationResult Validation<TResult>(IRequest<TResult> message)
        {
            using (var scope = ScopeFactory.CreateScope())
            {
                var serviceProvider = scope.ServiceProvider;

                var database = serviceProvider.GetService<DirectoryContext>();

                try
                {
                    database.BeginTransaction();
                    EmulateUserContextFilter(serviceProvider, database);

                    var validator = Validator(serviceProvider, message);

                    if (validator == null)
                        throw new Exception($"There is no validator for {message.GetType()} messages.");

                    var validationResult = validator.Validate(message);

                    database.CloseTransaction();

                    return validationResult;
                }
                catch (Exception exception)
                {
                    database.CloseTransaction(exception);
                    throw;
                }
            }
        }

        private static IValidator Validator<TResult>(IServiceProvider serviceProvider, IRequest<TResult> message)
        {
            var validatorType = typeof(IValidator<>).MakeGenericType(message.GetType());
            return serviceProvider.GetService(validatorType) as IValidator;
        }

        public static string SampleEmail()
        {
            return Guid.NewGuid() + "@example.com";
        }

        // The next two are helper methods to register and login

        // They will need to change based on your request types

        public static async Task<Employee> Register(Action<RegisterNewEmployee.ViewModel> customize = null)
        {
            var password = "password";
            var command = new RegisterNewEmployee.ViewModel()
            {
                FirstName = "Dustin",
                LastName = "Wells",
                JobTitle = "President & CEO",
                Office = Office.Austin,
                EmailAddress = SampleEmail(),
                PhoneNumber = "555-123-0001",
                Password = password,
                ConfirmPassword = password
            };

            customize?.Invoke(command);
            var employeeId = (await Send(command)).Id;
            return Query<Employee>(employeeId);
        }

        public static async Task<Employee> LogIn()
        {
            var email = SampleEmail();
            var password = "password";
            var employee = await Register(x =>
            {
                x.EmailAddress = email;
                x.Password = password;
                x.ConfirmPassword = password;
            });

            await Send(new AccountLogin.Command { EmailAddress = email, Password = password });
            return employee;
        }


    }
}