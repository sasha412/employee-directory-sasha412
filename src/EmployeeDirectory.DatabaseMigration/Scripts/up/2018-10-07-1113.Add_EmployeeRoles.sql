Create TABLE [dbo].[EmployeeRoles](
[Id] UNIQUEIDENTIFIER NOT NULL Constraint[DF_EmployeeRole_Id] DEFAULT NEWSEQUENTIALID(),
[EmployeeId] UNIQUEIDENTIFIER NOT NULL,
[RoleId] UNIQUEIDENTIFIER NOT NULL,
CONSTRAINT [PK_EmployeeRole] PRIMARY KEY CLUSTERED([Id]),
CONSTRAINT [Fk_EmployeeRoles_RoleID]FOREIGN KEY ([RoleId]) REFERENCES [dbo].[Roles]([Id]),
CONSTRAINT [Fk2_EmployeeRoles_EmployeeID]FOREIGN KEY ([EmployeeId]) REFERENCES [dbo].[Employee]([Id])
)