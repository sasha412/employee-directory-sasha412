DECLARE @EmployeeC UNIQUEIDENTIFIER;
SET @EmployeeC = NEWID();

DECLARE @EmployeeD UNIQUEIDENTIFIER;
SET @EmployeeD = NEWID();

DECLARE @RoleID UNIQUEIDENTIFIER;

INSERT INTO [dbo].[Employee] ([Id], [FirstName], [LastName], [JobTitle], [EmailAddress], [PhoneNumber], [Office],[HashedPassword]) VALUES (@EmployeeC, N'Kevin',N'Durant',N'Senior Consultant',N'kdurant@example.com',N'555-555-2222',3,'$2a$10$dteyZlAoQjApPMouJPQS4OpLsP/fTpsLbMrtNrZHfgBYAOokMa3fO');
INSERT INTO [dbo].[Employee] ([Id], [FirstName], [LastName], [JobTitle], [EmailAddress], [PhoneNumber], [Office],[HashedPassword]) VALUES (@EmployeeD, N'Stephen',N'Curry',N'Consultant',N'scurry@example.com',N'555-555-3333',3,'$2a$10$dteyZlAoQjApPMouJPQS4OpLsP/fTpsLbMrtNrZHfgBYAOokMa3fO');

select @RoleID = id from [dbo].[Roles] where [Name]='Manager';
Insert into [dbo].[EmployeeRoles]([EmployeeID],[RoleID]) values(@EmployeeC,@RoleID);
Insert into [dbo].[EmployeeRoles]([EmployeeID],[RoleID]) values(@EmployeeD,@RoleID);