DECLARE @EmployeeC UNIQUEIDENTIFIER;
SET @EmployeeC = NEWID();
INSERT INTO [dbo].[Employee] ([Id], [FirstName], [LastName], [JobTitle], [EmailAddress], [PhoneNumber], [Office],[HashedPassword]) VALUES (@EmployeeC, N'John',N'Cooper',N'Junior Consultant',N'jcooper@example.com',N'555-555-6789',3,'$2a$10$dteyZlAoQjApPMouJPQS4OpLsP/fTpsLbMrtNrZHfgBYAOokMa3fO');