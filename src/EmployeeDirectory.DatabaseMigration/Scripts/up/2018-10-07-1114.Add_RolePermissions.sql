Create TABLE [dbo].[RolePermissions](
[Id] UNIQUEIDENTIFIER NOT NULL Constraint[DF_RolePermission_Id] DEFAULT NEWSEQUENTIALID(),
[RoleID] UNIQUEIDENTIFIER NOT NULL,
[Permission] INT NOT NULL,
CONSTRAINT [PK_RolePermission] PRIMARY KEY CLUSTERED([Id]),
CONSTRAINT [Fk_RolePermissions_RoleID]FOREIGN KEY ([RoleId]) REFERENCES [dbo].[Roles]([Id])
)