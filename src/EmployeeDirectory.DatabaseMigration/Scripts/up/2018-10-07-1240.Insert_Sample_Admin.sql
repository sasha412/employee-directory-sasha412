DECLARE @EmployeeC UNIQUEIDENTIFIER;
SET @EmployeeC = NEWID();

DECLARE @EmployeeD UNIQUEIDENTIFIER;
SET @EmployeeD = NEWID();

DECLARE @RoleID UNIQUEIDENTIFIER;

INSERT INTO [dbo].[Employee] ([Id], [FirstName], [LastName], [JobTitle], [EmailAddress], [PhoneNumber], [Office],[HashedPassword]) VALUES (@EmployeeC, N'Ralph',N'Waldo',N'Consultant',N'rwaldo@example.com',N'555-555-1111',3,'$2a$10$dteyZlAoQjApPMouJPQS4OpLsP/fTpsLbMrtNrZHfgBYAOokMa3fO');
INSERT INTO [dbo].[Employee] ([Id], [FirstName], [LastName], [JobTitle], [EmailAddress], [PhoneNumber], [Office],[HashedPassword]) VALUES (@EmployeeD, N'Bob',N'Marley',N'Consultant',N'bmarley@example.com',N'555-555-4444',3,'$2a$10$dteyZlAoQjApPMouJPQS4OpLsP/fTpsLbMrtNrZHfgBYAOokMa3fO');

select @RoleID = id from [dbo].[Roles] where [Name]='System Administrator';
Insert into [dbo].[EmployeeRoles]([EmployeeID],[RoleID]) values(@EmployeeC,@RoleID);
Insert into [dbo].[EmployeeRoles]([EmployeeID],[RoleID]) values(@EmployeeD,@RoleID);
