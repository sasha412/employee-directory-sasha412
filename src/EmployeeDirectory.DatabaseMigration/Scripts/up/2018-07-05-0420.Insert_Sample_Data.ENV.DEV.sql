DECLARE @EmployeeA UNIQUEIDENTIFIER;
DECLARE @EmployeeB UNIQUEIDENTIFIER;
DECLARE @EmployeeC UNIQUEIDENTIFIER;

SET @EmployeeA = NEWID();
SET @EmployeeB = NEWID();
SET @EmployeeC = NEWID();

INSERT INTO [dbo].[Employee] ([Id], [FirstName], [LastName], [JobTitle], [EmailAddress], [PhoneNumber], [Office],[HashedPassword]) VALUES (@EmployeeA, N'Jane',N'Smith',N'Senior Consultant',N'smith@example.com',N'555-555-5555',1,'$2a$10$dteyZlAoQjApPMouJPQS4OpLsP/fTpsLbMrtNrZHfgBYAOokMa3fO');
INSERT INTO [dbo].[Employee] ([Id], [FirstName], [LastName], [JobTitle], [EmailAddress], [PhoneNumber], [Office],[HashedPassword]) VALUES (@EmployeeB, N'Robert',N'Johnson',N'Principle Consultant',N'john@example.com',N'555-555-1234',2,'$2a$10$dteyZlAoQjApPMouJPQS4OpLsP/fTpsLbMrtNrZHfgBYAOokMa3fO');
INSERT INTO [dbo].[Employee] ([Id], [FirstName], [LastName], [JobTitle], [EmailAddress], [PhoneNumber], [Office],[HashedPassword]) VALUES (@EmployeeC, N'Sally',N'Cooper',N'Junior Consultant',N'cooper@example.com',N'555-555-6789',3,'$2a$10$dteyZlAoQjApPMouJPQS4OpLsP/fTpsLbMrtNrZHfgBYAOokMa3fO');

