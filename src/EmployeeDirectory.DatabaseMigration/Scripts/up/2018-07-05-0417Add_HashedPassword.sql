Go
ALTER TABLE [dbo].[Employee]
        ADD [HashedPassword] nvarchar(256) NULL;
Go
Update [dbo].[Employee] set [HashedPassword] = '$2a$10$dteyZlAoQjApPMouJPQS4OpLsP/fTpsLbMrtNrZHfgBYAOokMa3fO';
Go
ALTER TABLE [dbo].[Employee] ALTER COLUMN [HashedPassword] nvarchar(256) NOT NULL;
