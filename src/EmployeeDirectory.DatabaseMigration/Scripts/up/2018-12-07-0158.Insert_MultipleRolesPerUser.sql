DECLARE @EmployeeC UNIQUEIDENTIFIER;
SET @EmployeeC = NEWID();

DECLARE @RoleIDA UNIQUEIDENTIFIER;
DECLARE @RoleIDB UNIQUEIDENTIFIER;

INSERT INTO [dbo].[Employee] ([Id], [FirstName], [LastName], [JobTitle], [EmailAddress], [PhoneNumber], [Office],[HashedPassword]) VALUES (@EmployeeC, N'Booker',N'T',N'Consultant',N'bt@example.com',N'555-559-1111',3,'$2a$10$dteyZlAoQjApPMouJPQS4OpLsP/fTpsLbMrtNrZHfgBYAOokMa3fO');

select @RoleIDA = id from [dbo].[Roles] where [Name]='Human Resources';
select @RoleIDB = id from [dbo].[Roles] where [Name]='Manager';
Insert into [dbo].[EmployeeRoles]([EmployeeID],[RoleID]) values(@EmployeeC,@RoleIDA);
Insert into [dbo].[EmployeeRoles]([EmployeeID],[RoleID]) values(@EmployeeC,@RoleIDB);
