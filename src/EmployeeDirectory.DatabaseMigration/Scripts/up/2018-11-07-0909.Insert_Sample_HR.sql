DECLARE @EmployeeC UNIQUEIDENTIFIER;
SET @EmployeeC = NEWID();

DECLARE @EmployeeD UNIQUEIDENTIFIER;
SET @EmployeeD = NEWID();

DECLARE @RoleID UNIQUEIDENTIFIER;

INSERT INTO [dbo].[Employee] ([Id], [FirstName], [LastName], [JobTitle], [EmailAddress], [PhoneNumber], [Office],[HashedPassword]) VALUES (@EmployeeC, N'Leonel',N'Messi',N'Senior Consultant',N'lmessi@example.com',N'555-555-3456',3,'$2a$10$dteyZlAoQjApPMouJPQS4OpLsP/fTpsLbMrtNrZHfgBYAOokMa3fO');
INSERT INTO [dbo].[Employee] ([Id], [FirstName], [LastName], [JobTitle], [EmailAddress], [PhoneNumber], [Office],[HashedPassword]) VALUES (@EmployeeD, N'Christiano',N'Ronaldo',N'Consultant',N'cronaldo@example.com',N'555-555-4789',3,'$2a$10$dteyZlAoQjApPMouJPQS4OpLsP/fTpsLbMrtNrZHfgBYAOokMa3fO');

select @RoleID = id from [dbo].[Roles] where [Name]='Human Resources';
Insert into [dbo].[EmployeeRoles]([EmployeeID],[RoleID]) values(@EmployeeC,@RoleID);
Insert into [dbo].[EmployeeRoles]([EmployeeID],[RoleID]) values(@EmployeeD,@RoleID);