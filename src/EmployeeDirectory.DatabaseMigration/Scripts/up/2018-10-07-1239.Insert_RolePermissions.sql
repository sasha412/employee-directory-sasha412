
DECLARE @RoleID UNIQUEIDENTIFIER;

-- Role Permissions
-- register = 1, edit =2, delete = 3, security = 4
select @RoleID = id from [dbo].[Roles] where [Name]='System Administrator';
Insert into [dbo].[RolePermissions]([RoleID],[Permission]) values(@RoleID,1);
Insert into [dbo].[RolePermissions]([RoleID],[Permission]) values(@RoleID,2);
Insert into [dbo].[RolePermissions]([RoleID],[Permission]) values(@RoleID,3);
Insert into [dbo].[RolePermissions]([RoleID],[Permission]) values(@RoleID,4);

select @RoleID = id from [dbo].[Roles] where [Name]='Manager';
Insert into [dbo].[RolePermissions]([RoleID],[Permission]) values(@RoleID,2);

select @RoleID = id from [dbo].[Roles] where [Name]='Human Resources';
Insert into [dbo].[RolePermissions]([RoleID],[Permission]) values(@RoleID,1);
Insert into [dbo].[RolePermissions]([RoleID],[Permission]) values(@RoleID,2);
Insert into [dbo].[RolePermissions]([RoleID],[Permission]) values(@RoleID,3);
