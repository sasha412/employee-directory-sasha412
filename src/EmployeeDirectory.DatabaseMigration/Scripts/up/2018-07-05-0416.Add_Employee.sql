Create TABLE [dbo].[Employee](
[Id] UNIQUEIDENTIFIER NOT NULL Constraint[DF_Employee_Id] DEFAULT NEWSEQUENTIALID(),
[FirstName] nvarchar(255) NOT NULL ,
[LastName] nvarchar(255) NOT NULL,
[JobTitle] nvarchar(255) NOT NULL,
[EmailAddress] nvarchar(255) NOT NULL,
[PhoneNumber] nvarchar(50) NULL,
[Office] int NOT NULL,
CONSTRAINT [PK_Employee] PRIMARY KEY CLUSTERED([Id])
)